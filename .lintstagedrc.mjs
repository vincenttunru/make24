import * as path from "path";

// See https://nextjs.org/docs/basic-features/eslint#lint-staged
const buildEslintCommand = (filenames) =>
`next lint --fix --file ${filenames
  .map((f) => path.relative(process.cwd(), f))
  .join(' --file ')}`;

export default {
  "*.{js,jsx,ts,tsx}": [buildEslintCommand],
  "*.{js,jsx,ts,tsx,css,md}": "prettier --write",
};
