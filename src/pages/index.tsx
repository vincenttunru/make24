import { ButtonAria } from "@react-aria/button";
import type { NextPage } from "next";
import Head from "next/head";
import {
  ButtonHTMLAttributes,
  RefObject,
  useMemo,
  useRef,
  useState,
  useEffect,
  useCallback,
} from "react";
import { useToggleButton } from "react-aria";
import { ToggleState, useToggleState } from "react-stately";
import { MdUndo } from "react-icons/md";

type Operation = "add" | "subtract" | "multiply" | "divide";
type Numbers = [number, number, number, number];
type Operations = [Operation, Operation, Operation];

function getOperation(twoDigitNumber: number): Operation {
  if (twoDigitNumber < 25) {
    return "add";
  }
  if (twoDigitNumber < 50) {
    return "subtract";
  }
  if (twoDigitNumber < 75) {
    return "multiply";
  }
  return "divide";
}

function applyOperation(
  operation: Operation,
  num1: number,
  num2: number
): number {
  switch (operation) {
    case "add":
      return num1 + num2;
    case "subtract":
      return num1 - num2;
    case "multiply":
      return num1 * num2;
    case "divide":
      return num1 / num2;
  }
}

function makes24(numbers: Numbers, operations: Operations): boolean {
  return (
    applyOperation(
      operations[0],
      applyOperation(
        operations[1],
        applyOperation(operations[2], numbers[0], numbers[1]),
        numbers[2]
      ),
      numbers[3]
    ) === 24
  );
}

function getPuzzleFromNumberString(numberString: string): Numbers | null {
  const numbers: Numbers = [
    parseInt(
      numberString.substring(numberString.length - 1, numberString.length),
      10
    ),
    parseInt(
      numberString.substring(numberString.length - 2, numberString.length - 1),
      10
    ),
    parseInt(
      numberString.substring(numberString.length - 3, numberString.length - 2),
      10
    ),
    parseInt(
      numberString.substring(numberString.length - 4, numberString.length - 3),
      10
    ),
  ];
  const operations: Operations = [
    getOperation(
      parseInt(
        numberString.substring(
          numberString.length - 6,
          numberString.length - 4
        ),
        10
      )
    ),
    getOperation(
      parseInt(
        numberString.substring(
          numberString.length - 8,
          numberString.length - 6
        ),
        10
      )
    ),
    getOperation(
      parseInt(
        numberString.substring(
          numberString.length - 10,
          numberString.length - 8
        ),
        10
      )
    ),
  ];
  const orderIndicators = [
    parseInt(
      numberString.substring(
        numberString.length - 12,
        numberString.length - 10
      ),
      10
    ),
    parseInt(
      numberString.substring(
        numberString.length - 13,
        numberString.length - 12
      ),
      10
    ),
    parseInt(
      numberString.substring(
        numberString.length - 14,
        numberString.length - 13
      ),
      10
    ),
  ] as const;
  for (let nrPermutation = 0; nrPermutation < 4; nrPermutation++) {
    for (let opPermutation = 0; opPermutation < 3; opPermutation++) {
      const nrsToTry: Numbers = [
        numbers[(nrPermutation + 0) % 4],
        numbers[(nrPermutation + 1) % 4],
        numbers[(nrPermutation + 2) % 4],
        numbers[(nrPermutation + 3) % 4],
      ];
      const opsToTry: Operations = [
        operations[(opPermutation + 0) % 3],
        operations[(opPermutation + 1) % 3],
        operations[(opPermutation + 2) % 3],
      ];

      if (makes24(nrsToTry, opsToTry)) {
        return orderNrs(nrsToTry, orderIndicators);
      }
    }
  }

  return null;
}

function orderNrs(
  nrs: Numbers,
  orderIndicators: readonly [number, number, number]
): Numbers {
  const firstNr = getFirstNr(orderIndicators[0]);
  const secondNr = getSecondNr(orderIndicators[1]);
  const thirdNr = getThirdNr(orderIndicators[2]);
  const newNrs = [nrs[firstNr]];
  let remainingNrs = nrs.filter((_nr, index) => index !== firstNr);
  newNrs.push(remainingNrs[secondNr]);
  remainingNrs = remainingNrs.filter((_nr, index) => index !== secondNr);
  newNrs.push(remainingNrs[thirdNr]);
  remainingNrs = remainingNrs.filter((_nr, index) => index !== thirdNr);
  newNrs.push(remainingNrs[0]);
  return newNrs as Numbers;
}

function getFirstNr(orderIndicator: number): ChosenNr {
  if (orderIndicator < 25) {
    return 0;
  }
  if (orderIndicator < 50) {
    return 1;
  }
  if (orderIndicator < 75) {
    return 2;
  }
  return 3;
}
function getSecondNr(orderIndicator: number): ChosenNr {
  if (orderIndicator < 3) {
    return 0;
  }
  if (orderIndicator < 6) {
    return 1;
  }
  return 2;
}
function getThirdNr(orderIndicator: number): ChosenNr {
  return orderIndicator < 5 ? 0 : 1;
}

/**
 * Deterministically (but chaotically) get a set of numbers that can be combined to form 24, based on today's date.
 * @returns Numbers that can be combined to form 24
 */
function getPuzzle(): Numbers {
  const now = Date.now();
  const nowDate = new Date(now);
  const multiplier =
    (nowDate.getUTCDay() + 1) *
    nowDate.getUTCDate() *
    (nowDate.getUTCMonth() + 1) *
    nowDate.getUTCFullYear();
  const todayTimestamp =
    Date.UTC(
      nowDate.getUTCFullYear(),
      nowDate.getUTCMonth(),
      nowDate.getUTCDate(),
      0,
      0,
      0
    ) / 100000;
  let numberString = todayTimestamp
    .toString()
    .split("")
    .map((nr) => parseInt(nr, 10) * multiplier)
    .join("");

  let i = 0;
  while (true) {
    const puzzle = getPuzzleFromNumberString(numberString);
    if (Array.isArray(puzzle)) {
      return puzzle;
    }
    i++;
    numberString = (parseInt(numberString, 10) + i).toString();
  }
}

type NrButtonData = {
  ref: RefObject<HTMLButtonElement>;
  state: ToggleState;
  button: ButtonAria<ButtonHTMLAttributes<HTMLButtonElement>>;
};

function useNrButtonData(
  props: Parameters<typeof useToggleState>[0]
): NrButtonData {
  const ref = useRef<HTMLButtonElement>(null);
  const state = useToggleState(props);
  const button = useToggleButton({}, state, ref);
  return { ref, state, button };
}

type ChosenNr = 0 | 1 | 2 | 3;
type Picks = Array<ChosenNr | Operation>;

function getNrPick(picks: Picks, round: number): ChosenNr | undefined {
  return picks[round * 2] as ChosenNr;
}
function getOpPick(picks: Picks, round: number): Operation | undefined {
  return picks[round * 2 + 1] as Operation;
}

type NrButtonProps = {
  data: NrButtonData;
  id: 0 | 1 | 2 | 3;
  value: number;
  picks: Picks;
  onChoose: () => void;
};
const NrButton = (props: NrButtonProps) => {
  const isDisabled = isNrDisabled(props.picks, props.id);
  return (
    <button
      {...props.data.button.buttonProps}
      ref={props.data.ref}
      disabled={isDisabled}
      className={
        "text-5xl bg-yellow-200 rounded-xl py-5 md:py-14 border-yellow-300 border-r-8 border-b-8 flex place-content-center " +
        (isDisabled
          ? "bg-gray-50 border-gray-100 "
          : " hover:bg-yellow-100 hover:border-yellow-200 ") +
        (props.data.state.isSelected
          ? "bg-yellow-50 border-yellow-100 active:bg-yellow-200 "
          : "active:bg-yellow-300")
      }
    >
      {props.value}
    </button>
  );
};

const OpButton = (props: ButtonHTMLAttributes<HTMLButtonElement>) => {
  return (
    <button
      {...props}
      className={
        "grow rounded-xl py-5 md:py-14 bg-yellow-200 border-yellow-300 border-r-4 border-b-4 md:border-r-8 md:border-b-8 flex place-content-center " +
        (props.disabled
          ? "bg-gray-50 border-gray-100 "
          : " hover:bg-yellow-100 hover:border-yellow-200 ")
      }
    >
      {props.children}
    </button>
  );
};

function isNrDisabled(picks: Picks, nr: ChosenNr): boolean {
  return picks.length % 2 === 1 || picks.includes(nr);
}

function getOutcome(puzzle: Numbers, picks: Picks): number {
  if (picks.length === 0) {
    return 0;
  }
  let outcome: number = getNrPick(picks, 0)!;
  for (let i = 1; i < picks.length; i = i + 2) {
    outcome = applyOperation(
      getOpPick(picks, i / 2)!,
      outcome,
      getNrPick(picks, i / 2 + 1)!
    );
  }
  return outcome;
}

function renderOp(operation: Operation): string {
  switch (operation) {
    case "add":
      return "+";
    case "subtract":
      return "-";
    case "multiply":
      return "x";
    case "divide":
      return "÷";
  }
}

const CalculationPart = (props: {
  emphasis: number;
  nr1?: number;
  nr2?: number;
  operation?: Operation;
}) => {
  if (typeof props.nr1 === "undefined") {
    return null;
  }

  const visibility = props.emphasis === 4 ? "inline" : "hidden md:inline";
  const size =
    props.emphasis === 4
      ? "text-8xl"
      : props.emphasis === 3
      ? "text-6xl"
      : props.emphasis === 2
      ? "text-4xl"
      : "text-xl";
  const colour =
    props.emphasis === 4
      ? "text-black"
      : props.emphasis === 3
      ? "text-slate-500"
      : props.emphasis === 2
      ? "text-slate-400"
      : "text-slate-300";

  return (
    <samp className={`${size} ${colour} ${visibility} transition-all`}>
      {props.nr1}
      {props.operation ? renderOp(props.operation) : null}
      {props.nr2 ?? null}
      {typeof props.nr2 !== "undefined" ? "=" : null}
    </samp>
  );
};
const Calculation = (props: { puzzle: Numbers; picks: Picks }) => {
  const nr1 = getNr(props.puzzle, getNrPick(props.picks, 0));
  const nr2 = getNr(props.puzzle, getNrPick(props.picks, 1));
  const nr3 = getNr(props.puzzle, getNrPick(props.picks, 2));
  const nr4 = getNr(props.puzzle, getNrPick(props.picks, 3));
  const intermediate1 =
    typeof nr1 !== "undefined" &&
    typeof nr2 !== "undefined" &&
    typeof props.picks[1] !== "undefined"
      ? applyOperation(getOpPick(props.picks, 0)!, nr1, nr2)
      : undefined;
  const intermediate2 =
    typeof intermediate1 !== "undefined" &&
    typeof nr3 !== "undefined" &&
    typeof props.picks[3] !== "undefined"
      ? applyOperation(getOpPick(props.picks, 1)!, intermediate1, nr3)
      : undefined;
  const endResult =
    typeof intermediate2 !== "undefined" &&
    typeof nr4 !== "undefined" &&
    typeof props.picks[5] !== "undefined"
      ? applyOperation(getOpPick(props.picks, 2)!, intermediate2, nr4)
      : undefined;

  const nrOfResults = [intermediate1, intermediate2, endResult].filter(
    (v) => typeof v === "number"
  ).length;

  return (
    <div className="p-2 flex items-center tracking-widest">
      <CalculationPart
        nr1={nr1}
        nr2={nr2}
        operation={getOpPick(props.picks, 0)}
        emphasis={4 - nrOfResults}
      />
      <CalculationPart
        nr1={intermediate1}
        nr2={nr3}
        operation={getOpPick(props.picks, 1)}
        emphasis={5 - nrOfResults}
      />
      <CalculationPart
        nr1={intermediate2}
        nr2={nr4}
        operation={getOpPick(props.picks, 2)}
        emphasis={6 - nrOfResults}
      />
      {typeof endResult !== "undefined" ? (
        <samp
          className={`text-black text-8xl ${
            endResult === 24 ? "text-green-700" : "text-red-700"
          }`}
        >
          {endResult}
        </samp>
      ) : null}
    </div>
  );
};

function getNr(puzzle: Numbers, chosenNr?: ChosenNr): number | undefined {
  return typeof chosenNr !== "undefined"
    ? puzzle[chosenNr] ?? undefined
    : undefined;
}

function lastPickIsNr(picks: Picks): boolean {
  return picks.length % 2 === 1;
}

const Home: NextPage = () => {
  const puzzle = useMemo(() => getPuzzle(), []);
  const [picks, setPicks] = useState<Picks>([]);

  const chooseNr = (nr: ChosenNr) => {
    if (lastPickIsNr(picks)) {
      return;
    }
    setPicks(picks.concat([nr]));
  };
  const getNrChangeHandler = (nr: ChosenNr) => {
    return (isSelected: boolean) => {
      if (isSelected) {
        chooseNr(nr);
      }
    };
  };

  const nrButtons = [
    useNrButtonData({
      isDisabled: isNrDisabled(picks, 0),
      onChange: getNrChangeHandler(0),
    }),
    useNrButtonData({
      isDisabled: isNrDisabled(picks, 1),
      onChange: getNrChangeHandler(1),
    }),
    useNrButtonData({
      isDisabled: isNrDisabled(picks, 2),
      onChange: getNrChangeHandler(2),
    }),
    useNrButtonData({
      isDisabled: isNrDisabled(picks, 3),
      onChange: getNrChangeHandler(3),
    }),
  ] as const;

  const undo = () => {
    if (lastPickIsNr(picks)) {
      nrButtons[picks[picks.length - 1] as ChosenNr].state.setSelected(false);
    }
    setPicks(picks.slice(0, -1));
  };

  const chooseOp = (op: Operation) => {
    if (!lastPickIsNr(picks)) {
      return;
    }
    setPicks(picks.concat([op]));
  };

  const onKeyDown = (event: KeyboardEvent) => {
    if (event.key === "Backspace") {
      event.preventDefault();
      undo();
    }
    if (event.key === "+") {
      event.preventDefault();
      chooseOp("add");
    }
    if (event.key === "-") {
      event.preventDefault();
      chooseOp("subtract");
    }
    if (event.key === "*") {
      event.preventDefault();
      chooseOp("multiply");
    }
    if (event.key === "/") {
      event.preventDefault();
      chooseOp("divide");
    }
    if (
      ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"].includes(event.key)
    ) {
      const nrPressed = parseInt(event.key, 10);
      const foundNr = puzzle.findIndex(
        (_nr, index) =>
          !picks.includes(index as ChosenNr) && puzzle[index] === nrPressed
      );
      if (foundNr >= 0) {
        event.preventDefault();
        chooseNr(foundNr as ChosenNr);
      }
    }
  };
  useEffect(() => {
    document.addEventListener("keydown", onKeyDown);

    return () => {
      document.removeEventListener("keydown", onKeyDown);
    };
  });

  return (
    <div className="bg-yellow-50 h-auto min-h-full">
      <Head>
        <title>Make 24</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="">
        <div className="flex flex-row-reverse">
          <div className="bg-blue-100 py-7 px-16 shadow-lg md:py-10 md:text-xl w-full flex">
            <div className={`grow flex flex-col justify-center`}>
              <h1 className="text-2xl font-bold pb-2">Make 24</h1>
              <p className="md:pt-2">
                Combine the four numbers to make 24. There&apos;s a new puzzle
                every day.
              </p>
            </div>
          </div>
        </div>
        <div className="grid grid-cols-2 gap-9 p-9">
          <NrButton
            data={nrButtons[0]}
            id={0}
            value={puzzle[0]}
            picks={picks}
            onChoose={() => chooseNr(0)}
          />
          <NrButton
            data={nrButtons[1]}
            id={1}
            value={puzzle[1]}
            picks={picks}
            onChoose={() => chooseNr(1)}
          />
          <NrButton
            data={nrButtons[2]}
            id={2}
            value={puzzle[2]}
            picks={picks}
            onChoose={() => chooseNr(2)}
          />
          <NrButton
            data={nrButtons[3]}
            id={3}
            value={puzzle[3]}
            picks={picks}
            onChoose={() => chooseNr(3)}
          />
        </div>
        <div className="flex gap-9 px-9 md:py-5">
          <OpButton
            onClick={() => chooseOp("add")}
            disabled={!lastPickIsNr(picks)}
          >
            ➕️
          </OpButton>
          <OpButton
            onClick={() => chooseOp("subtract")}
            disabled={!lastPickIsNr(picks)}
          >
            ➖️
          </OpButton>
          <OpButton
            onClick={() => chooseOp("multiply")}
            disabled={!lastPickIsNr(picks)}
          >
            ✖️
          </OpButton>
          <OpButton
            onClick={() => chooseOp("divide")}
            disabled={!lastPickIsNr(picks)}
          >
            ➗️
          </OpButton>
        </div>
        <div className="flex flex-row p-9 h-40 items-center">
          <span className="flex-grow">
            <Calculation puzzle={puzzle} picks={picks} />
          </span>
          <span className="flex items-center">
            <button
              className={`p-3 rounded-xl border-r-2 border-b-2 md:border-r-4 md:border-b-4 ${
                picks.length === 0
                  ? "bg-gray-50 border-gray-100"
                  : "bg-red-50 border-red-100"
              }`}
              onClick={() => undo()}
              disabled={picks.length === 0}
            >
              <MdUndo aria-label="Undo" />
            </button>
          </span>
        </div>
      </main>
    </div>
  );
};

export default Home;
